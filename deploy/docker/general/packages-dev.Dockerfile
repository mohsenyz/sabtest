FROM sabaide/php-base:1.0.0

LABEL Maintainer="Mohsen Yazdani <mohsenyz2000@gmail.com>"
LABEL Name="Packages Base Image"
LABEL Version="2023-10-13"

ENV COMPOSER_ALLOW_SUPERUSER=1

USER root
WORKDIR /app/

COPY composer.json composer.lock /app/
RUN  composer install --no-autoloader --no-scripts --no-cache
