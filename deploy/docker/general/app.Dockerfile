FROM sabaide/packages:1.0.0 AS pkg

ARG APP_ENV=production

COPY . /app/

RUN set -eux; \
    if [ "${APP_ENV}" = "staging" ] || [ "${APP_ENV}" = "production" ] ; \
    then \
    composer dumpautoload --optimize; \
    fi

FROM sabaide/php-base:1.0.0

ARG APP_ENV=production

USER root

WORKDIR /app/

COPY . /app/
COPY --from=pkg /app/vendor/ ./vendor/
COPY --from=pkg /app/public/ ./public/

RUN set -eux; \
    rm -f /usr/local/etc/php-fpm.d/* && \
    rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    chown -R $NONROOT_USER:$NONROOT_USER \
        /app \
        /usr/local/etc/php-fpm.d \
        /etc/nginx \
        /usr/local/etc/php/conf.d/ \
        /etc/supervisor/conf.d/
COPY ./deploy/docker/general/configs/supervisord.conf /etc/supervisor/supervisord.conf
COPY ./deploy/docker/general/configs/php-fpm.conf /usr/local/etc/php-fpm.conf

USER $NONROOT_USER

ENTRYPOINT ["/app/deploy/docker/general/scripts/entrypoint.sh"]
