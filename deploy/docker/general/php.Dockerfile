FROM php:8.2-fpm-bullseye
ENV COMPOSER_VERSION=2.6.5
ENV NONROOT_USER=www-data
ENV TZ "Asia/Tehran"

LABEL Maintainer="Mohsen Yazdani <mohsenyz2000@gmail.com>"
LABEL Name="PHP Base Image"
LABEL Version="2023-10-13"

USER root

RUN set -eux; \
    cp /usr/share/zoneinfo/${TZ} /etc/localtime && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
        git \
        curl \
        cron \
        wget \
        gnupg2 \
        ca-certificates \
        lsb-release \
        apt-transport-https \
        autoconf \
        nginx \
        supervisor \
        libzip4 \
        tzdata \
        netcat \
        telnet \
        vim \
        tmux \
        net-tools \
        procps \
        lsof \
        iputils-ping \
        traceroute \
        g++ \
        gcc \
        make \
        binutils \
        pkg-config \
        zlib1g-dev \
        libmcrypt-dev \
        libpng-dev \
        libxpm-dev \
        libjpeg-dev \
        libwebp-dev \
        libgmp-dev \
        libsodium-dev \
        libxml2-dev \
        libfreetype6-dev \
        libicu-dev \
        libzip-dev \
        libcurl4-openssl-dev \
        libssl-dev && \
    pecl channel-update pecl.php.net && \
    pecl install xdebug && \
    pecl config-set php_ini "$PHP_INI_DIR" && \
    docker-php-ext-install -j$(nproc) bcmath gmp pdo_mysql opcache intl sockets zip && \
    docker-php-ext-enable xdebug zip && \
    apt-get -y purge \
        g++ \
        gcc \
        make \
        binutils  \
        zlib1g-dev \
        libpng-dev \
        libxpm-dev \
        libjpeg-dev \
        libwebp-dev \
        libgmp-dev \
        libsodium-dev \
        libxml2-dev \
        libfreetype6-dev \
        libicu-dev \
        libzip-dev \
        libcurl4-openssl-dev \
        libssl-dev && \
    apt-get clean && \
    apt-get autoremove --yes && \
    rm -rf /tmp/* /var/lib/apt/lists/* && \
    curl -sS https://getcomposer.org/installer | php -- \
        --version=$COMPOSER_VERSION \
        --install-dir=/usr/bin --filename=composer


RUN set -eux; \
    mkdir -p /var/log/supervisor /etc/supervisor/conf.d/ && \
    mkdir -p /var/tmp/nginx /run/nginx/ && \
    # add non-root user
    usermod -u 1000 $NONROOT_USER && groupmod -g 1000 $NONROOT_USER && \
    # give permission to required path to the generated non-root
    mkdir -p /var/run/php/ && \
    mkdir -p /var/cache/nginx/ && \
    chown -R $NONROOT_USER:$NONROOT_USER /run/ /var/ && \
    # Allow none-root user run crond
    chmod gu+s /usr/sbin/cron

CMD [ "supervisord", "-c", "/etc/supervisor/supervisord.conf" ]
