#!/bin/bash
set -Eeuo pipefail
IFS=$'\n\t'


PHP_BASE_TAG_VERSION="1.0.0"
# Version 1.0.2

PACKAGES_TAG_VERSION="1.0.0"
# Version 1.0.2

readonly REGISTRY_URL="sabaide"
readonly ACTION_BUILD="build"
readonly ACTION_PUSH="push"
readonly IMAGE_PHP_BASE="php-base"
readonly IMAGE_PACKAGES="packages"
readonly IMAGE_PACKAGES_DEV="packages-dev"
readonly IMAGE_ALL="all"


buildBasePHPImage(){
    echo "==> Building $IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION ..."
    docker build --network host --no-cache -t $REGISTRY_URL/$IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION \
        -f deploy/docker/general/php.Dockerfile .
    echo "==> $IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION built successfully."
}

buildPackagesImage(){
    echo "==> Building $IMAGE_PACKAGES_DEV:$PACKAGES_TAG_VERSION ..."
    docker build --no-cache -t $REGISTRY_URL/$IMAGE_PACKAGES_DEV:$PACKAGES_TAG_VERSION \
        -f deploy/docker/general/packages-dev.Dockerfile .
    echo "==> $IMAGE_PACKAGES_DEV:$PACKAGES_TAG_VERSION built successfully."
}

pushBasePHPImage(){
    echo "==> Pushing $IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION ..."
    docker push $REGISTRY_URL/php-base:$PHP_BASE_TAG_VERSION
    echo "==> $IMAGE_PHP_BASE:$PHP_BASE_TAG_VERSION pushed successfully."
}

pushPackagesImage(){
    echo "==> Pushing $IMAGE_PACKAGES:$PACKAGES_TAG_VERSION ..."
    docker push $REGISTRY_URL/$IMAGE_PACKAGES:$PACKAGES_TAG_VERSION
    echo "==> $IMAGE_PACKAGES:$PACKAGES_TAG_VERSION pushed successfully."

    echo "==> Pushing $IMAGE_PACKAGES_DEV:$PACKAGES_TAG_VERSION ..."
    docker push $REGISTRY_URL/$IMAGE_PACKAGES_DEV:$PACKAGES_TAG_VERSION
    echo "==> $IMAGE_PACKAGES_DEV:$PACKAGES_TAG_VERSION pushed successfully."
}

# check for action & image
if [ $# -ge 2 ] && [ -n "$1" ] && [ -n "$2" ]
then
  export ACTION="$1"
  export IMAGE="$2"
else
    echo "usage: base_image_helper.sh [build | push] [php-base | packages | all]"
    exit
fi

# check for invalid 'action' & 'image'
if [ "${ACTION}" != "${ACTION_BUILD}" ] && [ "${ACTION}" != "${ACTION_PUSH}" ];
then
    echo "invalid action(valid: [build | push])"
    exit
fi

if [ "${IMAGE}" != "${IMAGE_PHP_BASE}" ] && [ "${IMAGE}" != "${IMAGE_PACKAGES}" ] && [ "${IMAGE}" != "${IMAGE_ALL}" ];
then
    echo "invalid image(valid: [php-base | packages | all])"
    exit
fi

case $ACTION in
  build)
      case $IMAGE in
        $IMAGE_PHP_BASE)
            buildBasePHPImage
            ;;
        $IMAGE_PACKAGES)
            buildPackagesImage
            ;;
        $IMAGE_ALL)
            buildBasePHPImage
            buildPackagesImage
            ;;
      esac
    ;;

  push)
    case $IMAGE in
        $IMAGE_PHP_BASE)
            pushBasePHPImage
            ;;
        $IMAGE_PACKAGES)
            pushPackagesImage
            ;;
        $IMAGE_ALL)
            pushBasePHPImage
            pushPackagesImage
            ;;
      esac
esac
