#!/usr/bin/env bash
set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "==> Finished running script, exiting"
}

echo "==> Provisioning database for local development"

php manage.php db:migrate
