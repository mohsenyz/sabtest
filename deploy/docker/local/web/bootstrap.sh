#!/usr/bin/env bash
set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  echo "==> Finished running script, exiting"
}

cd deploy/docker/local/web

# php
cp -v php-fpm/*.conf /usr/local/etc/php-fpm.d/
cp -v php/php.ini /usr/local/etc/php/conf.d/

# nginx
cp -v nginx/default.conf /etc/nginx/conf.d/default.conf
cp -v nginx/nginx.conf /etc/nginx/nginx.conf

cp -v supervisor.conf /etc/supervisor/conf.d/web.conf

/usr/bin/supervisord -c /etc/supervisor/supervisord.conf
