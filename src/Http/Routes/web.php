<?php

use App\Http\Controllers\RedirectBySlug;
use App\Http\Router;

Router::fallback(RedirectBySlug::class);