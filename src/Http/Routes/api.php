<?php

use App\Http\Controllers\Api\CreateLink;
use App\Http\Controllers\Api\CreateUser;
use App\Http\Controllers\Api\ListLinks;
use App\Http\Controllers\Api\LoginUser;
use App\Http\Middlewares\Api\Authenticate;
use App\Http\Router;

Router::post('/api/v1/links', [Authenticate::class, CreateLink::class]);
Router::get('/api/v1/links', [Authenticate::class, ListLinks::class]);
Router::post('/api/v1/auth/register', CreateUser::class);
Router::post('/api/v1/auth/login', LoginUser::class);