<?php

namespace App\Http;

class Response
{
    public function __construct(
        protected int    $httpCode = 200,
        protected mixed  $body = null,
        protected string $contentType = 'application/json',
    )
    {
        //
    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    public function setHttpCode(int $httpCode): void
    {
        $this->httpCode = $httpCode;
    }


    public function getBody(): ?string
    {
        return is_string($this->body) ? $this->body : json_encode($this->body);
    }

    public function setBody(array|string|null $body): void
    {
        $this->body = $body;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setContentType(string $contentType): void
    {
        $this->contentType = $contentType;
    }
}