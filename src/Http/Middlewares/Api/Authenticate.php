<?php

namespace App\Http\Middlewares\Api;

use App\Helpers\Auth;
use App\Helpers\JWT;
use App\Http\Exceptions\HttpException;
use App\Http\Exceptions\UnauthorizedException;
use App\Http\Request;
use App\Services\AuthService;

class Authenticate
{
    public function __invoke()
    {
        $authService = new AuthService();

        try {
            $userId = JWT::decodeForUser(Request::getBearerToken());
            $user = $authService->getUserById($userId);

            if (!$user) {
                throw new UnauthorizedException();
            }

            Auth::login($user);
        } catch (\Exception $exception) {
            throw new HttpException(401, 'unauthorized');
        }
    }
}