<?php

namespace App\Http\Exceptions;

class HttpException extends \Exception
{
    public function __construct(int $httpCode, string $message, ?\Throwable $previous = null)
    {
        parent::__construct($message, $httpCode, $previous);
    }

    public function render(): string
    {
        http_response_code($this->code);

        return json_encode([
            'message' => $this->message
        ]);
    }
}