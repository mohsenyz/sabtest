<?php

namespace App\Http;

use App\Http\Exceptions\HttpException;

class Request
{
    /**
     * @throws HttpException
     */
    public static function getJsonBody(): mixed
    {
        try {
            return json_decode(file_get_contents('php://input'), $associative = true, $depth = 20, JSON_THROW_ON_ERROR);
        } catch (\Exception $exception) {
            throw new HttpException(400, 'invalid json body');
        }
    }

    public static function getHost(): string
    {
        return $_SERVER['HTTP_HOST'];
    }

    public static function getBearerToken(): ?string
    {
        $authorization = trim($_SERVER['Authorization'] ?? $_SERVER['HTTP_AUTHORIZATION']);

        if (preg_match('/Bearer\s(\S+)/', $authorization, $matches)) {
            return $matches[1];
        }

        return null;
    }
}