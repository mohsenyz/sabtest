<?php

namespace App\Http;

use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

class Router
{
    protected static $registerdRoutes = [];

    protected static $fallbackHandler = null;

    public static function post(string $uri, array|string $handler): void
    {
        self::addRoute('POST', $uri, $handler);
    }

    private static function addRoute(string $method, string $uri, array|string $handler): void
    {
        if (!isset(self::$registerdRoutes[$uri])) {
            self::$registerdRoutes[$uri] = [];
        }

        self::$registerdRoutes[$uri][$method] = $handler;
    }

    public static function get(string $uri, array|string $handler): void
    {
        self::addRoute('GET', $uri, $handler);
    }

    public static function put(string $uri, array|string $handler): void
    {
        self::addRoute('PUT', $uri, $handler);
    }

    public static function patch(string $uri, array|string $handler): void
    {
        self::addRoute('PATCH', $uri, $handler);
    }

    public static function delete(string $uri, array|string $handler): void
    {
        self::addRoute('DELETE', $uri, $handler);
    }

    public static function fallback(string $handler): void
    {
        self::$fallbackHandler = $handler;
    }

    public static function getFallback(): ?string
    {
        return self::$fallbackHandler;
    }

    public static function hasFallback(): bool
    {
        return self::$fallbackHandler !== null;
    }

    public static function clear(): void
    {
        self::$registerdRoutes = [];
    }

    public static function dispatch(string $method, string $uri): array
    {
        $dispatcher = simpleDispatcher(function (RouteCollector $r) {
            foreach (self::$registerdRoutes as $uri => $methods) {
                foreach ($methods as $method => $handler) {
                    $r->addRoute($method, $uri, $handler);
                }
            }
        });

        return $dispatcher->dispatch($method, $uri);
    }
}