<?php

namespace App\Http;

use App\Boot\LoadConfig;
use App\Boot\LoadEnv;
use App\Boot\LoadRoutes;
use App\Helpers\Config;
use App\Http\Exceptions\HttpException;
use FastRoute\Dispatcher;

class Kernel
{
    protected $boostrappers = [
        LoadEnv::class,
        LoadConfig::class,
        LoadRoutes::class,
    ];

    public function bootstrap()
    {
        foreach ($this->boostrappers as $boostrapper) {
            (new $boostrapper)->boot();
        }
    }

    public function handle(string $method, string $uri)
    {
        $result = Router::dispatch($method, $uri);

        try {
            if ($result[0] === Dispatcher::NOT_FOUND) {
                if (Router::hasFallback()) {
                    $fallback = Router::getFallback();
                    (new $fallback)($method, $uri);
                } else {
                    throw new HttpException(400, 'resource not found');
                }
            }

            if ($result[0] === Dispatcher::METHOD_NOT_ALLOWED) {
                throw new HttpException(405, 'method not allowed');
            }

            $handlers = is_string($result[1]) ? [$result[1]] : $result[1];
            $result = null;

            foreach ($handlers as $handler) {
                $result = (new $handler)();
            }

            if (is_string($result)) {
                http_response_code(200);
                header('Content-type: text/html');
                echo $result;
            } else if ($result instanceof Response) {
                http_response_code($result->getHttpCode());
                header('Content-type: ' . $result->getContentType());
                echo $result->getBody();
            }
        } catch (HttpException $httpException) {
            header('Content-type: application/json');
            echo $httpException->render();
        } catch (\Throwable $throwable) {
            if (Config::get('app.debug', false)) {
                throw $throwable;
            }

            header('Content-type: application/json');
            echo (new HttpException(500, 'internal server error'))->render();
        }
    }
}