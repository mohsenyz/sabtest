<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Auth;
use App\Http\Response;
use App\Services\LinkService;

class ListLinks
{
    public function __invoke()
    {
        $linkService = new LinkService();
        $links = $linkService->getLinksByUser(Auth::user()->id);

        return new Response(body: array_map(function ($link) {
            return [
                'id' => $link->id,
                'slug' => $link->slug,
                'original_link' => $link->original_link,
            ];
        }, $links));
    }
}