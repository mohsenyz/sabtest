<?php

namespace App\Http\Controllers\Api;

use App\Http\Exceptions\HttpException;
use App\Http\Exceptions\UserAlreadyExistsException;
use App\Http\Request;
use App\Http\Response;
use App\Services\AuthService;

class CreateUser
{
    public function __invoke()
    {
        $body = Request::getJsonBody();
        $username = $body['username'] ?? null;
        $password = $body['password'] ?? null;

        if (!preg_match('/^\w{5,}$/', $username)) {
            throw new HttpException(400, 'username should be alphanumeric more than 5 characters');
        }

        if (!is_string($password) || mb_strlen($password) < 8) {
            throw new HttpException(400, 'password must be string and >= 8 characters');
        }

        $authService = new AuthService();

        try {
            $authService->createUser($username, $password);
        } catch (UserAlreadyExistsException $exception) {
            throw new HttpException(400, 'user already exists');
        }

        return new Response(body: [
            'message' => 'user created successfully'
        ]);
    }
}