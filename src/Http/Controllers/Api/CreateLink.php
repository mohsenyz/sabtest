<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Auth;
use App\Http\Exceptions\HttpException;
use App\Http\Exceptions\UserAlreadyExistsException;
use App\Http\Request;
use App\Http\Response;
use App\Services\LinkService;

class CreateLink
{
    public function __invoke()
    {
        $body = Request::getJsonBody();
        $link = $body['link'] ?? null;

        if (!filter_var($link ?? null, FILTER_VALIDATE_URL) ||
            !in_array(parse_url($link)['scheme'], ['http', 'https'])) {
            throw new HttpException(400, 'invalid link provided');
        }

        $linkService = new LinkService();

        try {
            $slug = $linkService->createLink($link, user: Auth::user());

            return new Response(body: [
                'message' => 'link created',
                'link' => Request::getHost() . '/' . $slug
            ]);
        } catch (UserAlreadyExistsException $exception) {
            throw new HttpException(400, 'user already exists');
        }

    }
}