<?php

namespace App\Http\Controllers;

use App\Http\Exceptions\HttpException;
use App\Services\LinkService;

class RedirectBySlug
{
    public function __invoke(string $method, string $uri)
    {
        if ($method !== 'GET') {
            throw new HttpException(405, 'method not allowed');
        }

        $slug = trim($uri, '/');
        $linkService = new LinkService();
        $link = $linkService->getLinkBySlug($slug);

        if (!$link) {
            throw new HttpException(404, 'resource not found');
        }

        http_response_code(302);
        header('Location: ' . $link->original_link);
        echo 'You will be redirected to ' . $link->original_link;
    }
}