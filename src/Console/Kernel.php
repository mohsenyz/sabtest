<?php

namespace App\Console;

use App\Boot\LoadConfig;
use App\Boot\LoadEnv;

class Kernel
{
    protected static $commands = [
        'db:migrate' => MigrateDatabase::class,
    ];

    protected $boostrappers = [
        LoadEnv::class,
        LoadConfig::class,
    ];

    public function bootstrap()
    {
        foreach ($this->boostrappers as $boostrapper) {
            (new $boostrapper)->boot();
        }
    }

    public function handle(array $args)
    {
        $command = array_shift($args);

        if (isset(self::$commands[$command])) {
            (new self::$commands[$command])($args);
            return;
        }

        die('command not found');
    }
}