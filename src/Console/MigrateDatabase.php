<?php

namespace App\Console;

use App\Helpers\Database;

class MigrateDatabase
{
    public function __invoke(?array $args)
    {
        $database = Database::defaultConnection();

        $migrations = <<<SQL
        CREATE TABLE IF NOT EXISTS users (
            id INT AUTO_INCREMENT PRIMARY KEY,
            username VARCHAR(255) UNIQUE NOT NULL,
            password VARCHAR(255) NOT NULL,
            active TINYINT(1) DEFAULT 1
        );

        CREATE TABLE IF NOT EXISTS links (
            id INT AUTO_INCREMENT PRIMARY KEY,
            slug VARCHAR(255) UNIQUE DEFAULT NULL,
            original_link TEXT NOT NULL,
            user_id INT DEFAULT NULL,
            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            FOREIGN KEY (user_id) REFERENCES users(id)
        );
        SQL;

        $database->execute($migrations);

        echo 'Tables created';
    }
}