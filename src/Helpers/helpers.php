<?php

function env(string $env, string $default = null)
{
    return $_ENV[$env] ?? $default;
}