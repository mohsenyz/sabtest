<?php

namespace App\Helpers;

class Auth
{
    protected static ?object $user = null;

    public static function login(object $user)
    {
        self::$user = $user;
    }

    public static function user(): ?object
    {
        return self::$user;
    }
}