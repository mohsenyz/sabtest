<?php

namespace App\Helpers;

use PDO;
use PDOStatement;

class Database
{
    protected static Database|null $defaultInstance = null;

    private PDO $pdo;

    public function __construct(
        protected string $host,
        protected int    $port,
        protected string $username,
        protected string $password,
        protected string $database
    )
    {
        $this->connect();
    }

    private function connect()
    {
        try {
            $this->pdo = new PDO("mysql:host={$this->host};port={$this->port};dbname={$this->database}", $this->username, $this->password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            throw new \RuntimeException("Failed to connect to database");
        }
    }

    public static function defaultConnection(): Database
    {
        if (!self::$defaultInstance) {
            self::$defaultInstance = new Database(
                Config::get('db.host'),
                Config::get('db.port'),
                Config::get('db.username'),
                Config::get('db.password'),
                Config::get('db.database'),
            );
        }

        return self::$defaultInstance;
    }

    public function select($sql, $params = []): array|false
    {
        $stmt = $this->runQuery($sql, $params);
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function runQuery($sql, $params = []): PDOStatement
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt;
    }

    public function execute($sql): false|int
    {
        return $this->pdo->exec($sql);
    }

    public function getLastInsertedId(): false|string
    {
        return $this->pdo->lastInsertId();
    }
}