<?php

namespace App\Helpers;

use Firebase\JWT\Key;

class JWT
{
    public static function createTokenForUser(object $user): string
    {
        $now = new \DateTime('now');

        return \Firebase\JWT\JWT::encode([
            'sub' => $user->id,
            'iat' => $now->getTimestamp(),
            'nbf' => $now->getTimestamp(),
            'exp' => self::getExpirationDateFromNow(),
        ], self::getKey(), 'HS256');
    }

    private static function getExpirationDateFromNow(): int
    {
        $now = new \DateTime('now');
        $now->add(Config::get('jwt.exp'));

        return $now->getTimestamp();
    }

    private static function getKey()
    {
        return Config::get('jwt.key');
    }

    public static function decodeForUser(string $jwt): int
    {
        return \Firebase\JWT\JWT::decode($jwt, new Key(self::getKey(), 'HS256'))->sub;
    }
}