<?php

namespace App\Helpers;

use Sqids\Sqids;

class Hashids
{
    public static function encode(int $id): string
    {
        return self::getInstance()->encode([$id]);
    }

    private static function getInstance(): Sqids
    {
        return new Sqids(
            Config::get('hashids.alphabet'),
            Config::get('hashids.min_length'),
        );
    }

    public static function decode(string $hash): ?int
    {
        return self::getInstance()->decode($hash)[0] ?? null;
    }
}