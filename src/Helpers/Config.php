<?php

namespace App\Helpers;

class Config
{
    protected static $configs = [];

    public static function init(array $configs)
    {
        self::$configs = $configs;
    }

    public static function get($path, $value = null)
    {
        $paths = explode('.', $path);
        $result = self::$configs;
        foreach ($paths as $path) {
            $result = $result[$path];
        }

        return $result ?? $value;
    }
}