<?php

namespace App\Boot;

use Dotenv\Dotenv;

class LoadEnv implements Bootstrapper
{
    public function boot(): void
    {
        $dotenv = Dotenv::createImmutable(dirname(__DIR__, 2));
        $dotenv->safeLoad();
    }
}