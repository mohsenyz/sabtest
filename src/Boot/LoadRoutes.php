<?php

namespace App\Boot;

class LoadRoutes implements Bootstrapper
{
    public function boot(): void
    {
        require __DIR__ . '/../Http/Routes/api.php';
        require __DIR__ . '/../Http/Routes/web.php';
    }
}