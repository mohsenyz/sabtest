<?php

namespace App\Boot;

use App\Helpers\Config;

class LoadConfig implements Bootstrapper
{

    public function boot(): void
    {
        $hashids = require __DIR__ . '/../../config/hashids.php';
        $db = require __DIR__ . '/../../config/db.php';
        $jwt = require __DIR__ . '/../../config/jwt.php';
        $app = require __DIR__ . '/../../config/app.php';

        Config::init(
            compact('hashids', 'db', 'jwt', 'app'),
        );
    }
}