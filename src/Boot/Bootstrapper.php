<?php

namespace App\Boot;

interface Bootstrapper
{
    public function boot(): void;
}