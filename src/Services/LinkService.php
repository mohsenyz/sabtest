<?php

namespace App\Services;

use App\Helpers\Database;
use App\Helpers\Hashids;
use App\Http\Exceptions\LinkAlreadyExists;

class LinkService
{
    public function createLink(string $link, string $slug = null, object $user = null): string
    {
        $db = Database::defaultConnection();

        try {
            $db->runQuery('INSERT INTO links (original_link, slug, user_id) VALUES (:link, :slug, :user_id)', [
                'link' => $link,
                'slug' => $slug,
                'user_id' => $user?->id,
            ]);

            if (!$slug) {
                $id = $db->getLastInsertedId();
                $slug = Hashids::encode($id);
                $db->runQuery('UPDATE links SET slug = :slug WHERE id = :id', [
                    'id' => $id,
                    'slug' => $slug,
                ]);
            }

            return $slug;
        } catch (\PDOException $exception) {
            if ($exception->errorInfo[1] == 1062) {
                throw new LinkAlreadyExists();
            }

            throw $exception;
        }
    }

    public function getLinkBySlug(string $slug): ?object
    {
        $db = Database::defaultConnection();

        $links = $db->select('SELECT * FROM links WHERE slug = :slug', [
            'slug' => $slug,
        ]);

        return $links[0] ?? null;
    }

    public function getLinksByUser(int $userId): array
    {
        $db = Database::defaultConnection();

        return $db->select('SELECT * FROM links WHERE user_id = :user_id', [
            'user_id' => $userId,
        ]);
    }
}