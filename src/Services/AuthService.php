<?php

namespace App\Services;

use App\Helpers\Database;
use App\Helpers\JWT;
use App\Http\Exceptions\UnauthorizedException;
use App\Http\Exceptions\UserAlreadyExistsException;

class AuthService
{
    public function createUser(string $username, string $password)
    {
        $db = Database::defaultConnection();

        $passwordHash = password_hash($password, PASSWORD_DEFAULT);

        try {
            $db->runQuery('INSERT INTO users (username, password, active) VALUES (:username, :password, 1)', [
                'username' => $username,
                'password' => $passwordHash,
            ]);
        } catch (\PDOException $exception) {
            if ($exception->errorInfo[1] == 1062) {
                throw new UserAlreadyExistsException();
            }

            throw $exception;
        }
    }

    public function login(string $username, string $password): string
    {
        return JWT::createTokenForUser(
            $this->loginWithoutToken($username, $password)
        );
    }

    public function loginWithoutToken(string $username, string $password): object
    {
        $db = Database::defaultConnection();

        $users = $db->select('SELECT * FROM users WHERE username = :username', [
            'username' => $username,
        ]);

        if (count($users) !== 1) {
            throw new UnauthorizedException();
        }

        $user = $users[0];
        if ($user->active === 0) {
            throw new UnauthorizedException();
        }

        if (!password_verify($password, $user->password)) {
            throw new UnauthorizedException();
        }

        return $user;
    }

    public function getUserById(int $id): ?object
    {
        $db = Database::defaultConnection();

        $users = $db->select('SELECT * FROM users WHERE id = :id and active = 1', [
            'id' => $id,
        ]);

        return $users[0] ?? null;
    }

}