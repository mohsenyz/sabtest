<?php

require '../vendor/autoload.php';

error_reporting(E_ERROR | E_PARSE);

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = rawurldecode(strtok($_SERVER["REQUEST_URI"], '?'));

$kernel = new \App\Http\Kernel();
$kernel->bootstrap();
$kernel->handle($httpMethod, $uri);