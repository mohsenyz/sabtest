# Sabaide project

## How to setup

For setting the project up for local development, run below instructions:

First build base images
> Please docker buildkit using export DOCKER_BUILDKIT=0.

```shell
make build-base
```

Then

```shell
make env
```

Then open `.env` and setup values. after that run

```shell
make up
make provision-all
```

Congratulations! web project is up at 127.0.0.1:8000
