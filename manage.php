<?php

require 'vendor/autoload.php';

$kernel = new \App\Console\Kernel();
$kernel->bootstrap();
array_shift($argv);
$kernel->handle($argv);