<?php

return [
    'key' => env('JWT_KEY', 'fwegweftjggmiweksev'),
    'exp' => DateInterval::createFromDateString(env('JWT_EXP', '1 hour')),
];