<?php

return [
    'alphabet' => env('HASHID_ALPHABET', 'FxnXM1kBN6cuhsAvjW3C72RePyY8DwaU4Tzt9fHQrqSVKdpmLGIJgb5ZE'),
    'min_length' => env('HASHID_MIN_LENGTH', 5),
];