<?php

return [
    'host' => env('DB_HOST', '127.0.0.1'),
    'port' => env('DB_PORT', 3306),
    'username' => env('DB_USERNAME', 'sabaide'),
    'password' => env('DB_PASSWORD', 'sabaide'),
    'database' => env('DB_DATABASE', 'sabaide'),
];